package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (inputNumbers == null) 
            throw new CannotBuildPyramidException();
        int numOfLines = countNumberOfLines(inputNumbers.size());
        if(numOfLines == -1) 
            throw new CannotBuildPyramidException();
        int numOfColumns = countNumberOfColumns(inputNumbers.size());
        try {
            Collections.sort(inputNumbers);
        }catch (NullPointerException e) {throw new CannotBuildPyramidException();}
        int[][] matrix = new int [numOfLines][numOfColumns];

        int index = 0;
        for(int i = 0; i < matrix.length; i++)
        {
            int counter = 0;
            int firstInd = numOfColumns - numOfLines - i;
            int numberOfElementsInLine = i + 1;
            for(int j = firstInd; counter != numberOfElementsInLine; j += 2)
            {
                matrix[i][j] = inputNumbers.get(index++);
                ++counter;
            }
        }
        return matrix;
    }

    private int countNumberOfLines(int numberOfElements)
    {
        int a = 1, b = 1, c = numberOfElements * -2;
        int d = (int)Math.pow(b, 2) - 4 * a * c;
        double x = (-b + Math.sqrt(d)) / (2 * a);
        if(x - (int)x != 0) return -1;
        else return (int)x;
    }

    private int countNumberOfColumns(int numberOfElements)
    {
        for(int i = 1; numberOfElements - i != 0; i++)
            numberOfElements -= i;

        return numberOfElements + numberOfElements - 1;
    }


}
