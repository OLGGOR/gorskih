package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null) return null;
        statement = statement.replace(" ", "");
        if (statement.equals("")) return null;
        if (!isValid(statement)) return null;

        if (statement.contains("(")) {
            statement = calculateStatementWithPar(statement);
            if (statement == null) return null;
        }

        String finalRes = calculateStatement(statement);
        if (finalRes == null) return null;
        return formatResult(finalRes);
    }

    //возвращает результат в нужном формате
    private String formatResult(String statement) {
        if (statement.matches("\\-?\\d+\\.0+"))
            return statement.substring(0, statement.indexOf("."));
        return String.valueOf(Math.round(Double.parseDouble(statement) * 10_000) / 10000f);
    }

    //проверяет валидность выражения
    private boolean isValid(String statement) {
        Stack<Character> leftParentheses = new Stack<>();
        for (int i = 0; i < statement.length(); i++) {
            char ch = statement.charAt(i);
            if (!String.valueOf(ch).matches("[\\-+*/.\\d()]"))
                return false;
            if (i > 0 && String.valueOf(ch).matches("[\\-+*/.]")
                    && statement.charAt(i - 1) == ch) return false;
            if (ch == '(')
                leftParentheses.push(ch);
            else if (ch == ')') {
                if (!leftParentheses.empty())
                    leftParentheses.pop();
                else return false;
            }
        }
        return leftParentheses.empty();
    }

    //принимает выражение со скобками и возвращает упрощенное выражение без скобок
    private String calculateStatementWithPar(String statement) {
        Stack<Integer> leftParentheses = new Stack<>();
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') leftParentheses.push(i);
            else if (statement.charAt(i) == ')') {
                if (leftParentheses.empty() || leftParentheses.peek() == -1) return null;
                String substrWithParentheses = statement.substring(leftParentheses.peek(), statement.indexOf(')') + 1);
                String substrWithoutParentheses = statement.substring(leftParentheses.pop() + 1, statement.indexOf(')'));
                String res = calculateStatement(substrWithoutParentheses);
                if (res == null) return null;
                statement = statement.replace(substrWithParentheses, res);

                int rightIndex = statement.indexOf(')') - 1;
                if (leftParentheses.size() == 0)
                    leftParentheses.push(statement.indexOf('('));
                if (rightIndex + 1 < leftParentheses.peek()) return null;

                i = leftParentheses.peek();
            }
        }
        return statement;
    }

    //принимает выражение без скобок и возвращает результат вычисления
    private String calculateStatement(String statement) {
        if (statement.contains("*") || statement.contains("/")) {
            for (int i = 1; i < statement.length(); i++) {
                if (statement.charAt(i) == '*' || statement.charAt(i) == '/') {
                    statement = calculateTwoArguments(statement, i);
                    if (statement == null) return null;
                    i = 0;
                }
            }
        }
        if (statement.contains("-") || statement.contains("+")) {
            for (int i = 1; i < statement.length(); i++) {
                if (statement.charAt(i) == '-' || statement.charAt(i) == '+') {
                    statement = calculateTwoArguments(statement, i);
                    if (statement == null) return null;
                    i = 0;
                }
            }
        }
        return statement;
    }

    //расчитывает часть выражения, принадлежащую оператору на который указывает переданный индекс
    private String calculateTwoArguments(String statement, int i) {
        String operator = String.valueOf(statement.charAt(i));
        int j = i++ - 1;
        if (statement.charAt(i) == '-') ++i;
        while (i < statement.length() && String.valueOf(statement.charAt(i)).matches("[\\d.]")) ++i;
        while (j > 0 && String.valueOf(statement.charAt(j - 1)).matches("[\\-\\d.]")) --j;
        String res = operation(statement.substring(j, i), operator);
        if (res == null) return null;
        return statement.replace(statement.substring(j, i), res);
    }

    //принимает выражение состоящее из 2-х операндов и оператора
    //возвращает результат вычисления
    private String operation(String statement, String operation) {
        try {
            statement = statement.replace(operation, " ");
            String[] operands = statement.split(" ");
            double operand1 = Double.parseDouble(operands[0]);
            double operand2 = Double.parseDouble(operands[1]);
            switch (operation) {
                case "/":
                    if (operand2 == 0) return null;
                    return String.valueOf(operand1 / operand2);
                case "*":
                    return String.valueOf(operand1 * operand2);
                case "-":
                    return String.valueOf(operand1 - operand2);
                case "+":
                    return String.valueOf(operand1 + operand2);
                default:
                    return null;
            }
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
