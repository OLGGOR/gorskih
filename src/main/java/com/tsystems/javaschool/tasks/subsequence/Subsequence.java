package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {


    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        if(x == null || y == null) throw new IllegalArgumentException();
        if(x.size() == 0) return true;
        if (!y.containsAll(x)) return false;

        Object desiredElement = x.get(0);
        int coincidence = 0;
        for(int j = 0; j < y.size(); j++)
        {
            if(y.get(j) == null || desiredElement == null) return false;
            if(y.get(j).equals(desiredElement))
            {
                ++coincidence;
                int nextElementIndex = x.indexOf(desiredElement) + 1;
                if(nextElementIndex == x.size()) break;
                desiredElement = x.get(nextElementIndex);
            }
        }

        return coincidence == x.size();
    }
}
